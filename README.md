# Disclaimer Management API #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This application is in charge of management disclaimer acceptance of terms and conditions

### How do I get set up? ###

* You need to set in your system:
    * docker
    * java 8

### How do I run it locally? ###

* _mvn clean install_
* Go to docker folder and run: _docker compose up_
* Execute dynamo creation scripts from _util_commands.txt_


### How to use the API ###

* Go to swagger(http://localhost:8080/swagger-ui.html)


### Who do I talk to? ###

* delmoralcristian@gmail.com