package fivvy.delmoralcristian.disclaimer.repository;

import fivvy.delmoralcristian.disclaimer.model.Acceptance;
import org.socialsignin.spring.data.dynamodb.repository.DynamoDBCrudRepository;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;

import java.util.List;

@EnableScan
public interface AcceptanceRepository extends DynamoDBCrudRepository<Acceptance, String> {

    List<Acceptance> findByUserId(Long userId);

}
