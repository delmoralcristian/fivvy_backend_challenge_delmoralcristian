package fivvy.delmoralcristian.disclaimer.repository;

import fivvy.delmoralcristian.disclaimer.model.Disclaimer;
import org.socialsignin.spring.data.dynamodb.repository.DynamoDBCrudRepository;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;

import java.util.List;

@EnableScan
public interface DisclaimerRepository extends DynamoDBCrudRepository<Disclaimer, String> {

    List<Disclaimer> findByText(String text);

}
