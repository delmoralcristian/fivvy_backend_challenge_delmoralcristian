package fivvy.delmoralcristian.disclaimer.enums;

public enum CommonMessage {

    DISCLAIMER_NOT_FOUND("Disclaimer not found - Id: '%s'");

    private String message;

    CommonMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}
