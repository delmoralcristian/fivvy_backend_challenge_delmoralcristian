package fivvy.delmoralcristian.disclaimer.dto.response;

import fivvy.delmoralcristian.disclaimer.model.Disclaimer;
import lombok.*;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DisclaimerResponseDTO {

    private String id;
    private String name;
    private String text;
    private Long version;
    private Date createdAt;
    private Date updatedAt;

    public static DisclaimerResponseDTO of(Disclaimer disclaimer) {
        return DisclaimerResponseDTO.builder()
                .id(disclaimer.getId())
                .name(disclaimer.getName())
                .text(disclaimer.getText())
                .version(disclaimer.getVersion())
                .createdAt(disclaimer.getCreatedAt())
                .updatedAt(disclaimer.getUpdatedAt())
                .build();
    }

    public static List<DisclaimerResponseDTO> of(List<Disclaimer> disclaimers) {
        return disclaimers.stream()
                .map(disclaimer -> DisclaimerResponseDTO.of(disclaimer))
                .collect(Collectors.toList());
    }




}
