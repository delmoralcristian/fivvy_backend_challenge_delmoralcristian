package fivvy.delmoralcristian.disclaimer.dto.response;

import fivvy.delmoralcristian.disclaimer.model.Acceptance;
import lombok.*;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AcceptanceResponseDTO {

    private String id;
    private String disclaimerId;
    private Long userId;
    private Date createdAt;

    public static AcceptanceResponseDTO of(Acceptance acceptance) {
        return AcceptanceResponseDTO.builder()
                .id(acceptance.getId())
                .disclaimerId(acceptance.getDisclaimerId())
                .userId(acceptance.getUserId())
                .createdAt(acceptance.getCreatedAt())
                .build();
    }

    public static List<AcceptanceResponseDTO> of(List<Acceptance> acceptances) {
        return acceptances.stream()
                .map(acceptance -> AcceptanceResponseDTO.of(acceptance))
                .collect(Collectors.toList());
    }
}
