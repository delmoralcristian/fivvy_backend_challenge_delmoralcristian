package fivvy.delmoralcristian.disclaimer.dto.request;

import lombok.*;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DisclaimerRequestDTO {
    @NotNull
    private String name;
    @NotNull
    private String text;
    @NotNull
    private Long version;


}
