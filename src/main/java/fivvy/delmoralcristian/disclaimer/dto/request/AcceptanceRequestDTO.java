package fivvy.delmoralcristian.disclaimer.dto.request;

import lombok.*;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AcceptanceRequestDTO {
    @NotNull
    private String disclaimerId;
    @NotNull
    private Long userId;

}
