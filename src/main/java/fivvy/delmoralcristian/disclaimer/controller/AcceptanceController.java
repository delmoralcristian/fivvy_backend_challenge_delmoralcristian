package fivvy.delmoralcristian.disclaimer.controller;

import fivvy.delmoralcristian.disclaimer.dto.request.AcceptanceRequestDTO;
import fivvy.delmoralcristian.disclaimer.dto.response.AcceptanceResponseDTO;
import fivvy.delmoralcristian.disclaimer.model.Acceptance;
import fivvy.delmoralcristian.disclaimer.service.AcceptanceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/acceptance")
@Api(value = "Acceptance Controller")
public class AcceptanceController {

    @Autowired
    AcceptanceService acceptanceService;

    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    @ApiOperation(value = "Create acceptance")
    public ResponseEntity<AcceptanceResponseDTO> createAcceptance(@RequestBody AcceptanceRequestDTO body) {
        Acceptance acceptance = this.acceptanceService.create(body.getDisclaimerId(), body.getUserId());
        return new ResponseEntity<>(AcceptanceResponseDTO.of(acceptance), HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "Get all acceptance by user id")
    public ResponseEntity<List<AcceptanceResponseDTO>> getAllAcceptanceByUserId(@RequestParam(value = "userId", required = false) Long userId) {
        return new ResponseEntity<>(AcceptanceResponseDTO.of(this.acceptanceService.getAllByUserId(userId)), HttpStatus.OK);
    }


}
