package fivvy.delmoralcristian.disclaimer.controller;

import fivvy.delmoralcristian.disclaimer.dto.request.DisclaimerRequestDTO;
import fivvy.delmoralcristian.disclaimer.dto.response.DisclaimerResponseDTO;
import fivvy.delmoralcristian.disclaimer.model.Disclaimer;
import fivvy.delmoralcristian.disclaimer.service.DisclaimerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/disclaimer")
@Api(value = "Disclaimer Controller")
public class DisclaimerController {

    @Autowired
    DisclaimerService disclaimerService;

    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    @ApiOperation(value = "Create disclaimer")
    public ResponseEntity<DisclaimerResponseDTO> createDisclaimer(@RequestBody DisclaimerRequestDTO body) {
        Disclaimer disclaimer = this.disclaimerService.create(body.getName(), body.getText(), body.getVersion());
        return new ResponseEntity<>(DisclaimerResponseDTO.of(disclaimer), HttpStatus.CREATED);
    }
    @RequestMapping(value = "/id/{id}", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "Get disclaimer by id")
    public ResponseEntity<DisclaimerResponseDTO> getDisclaimerById(@PathVariable String id) {
        return new ResponseEntity<>(DisclaimerResponseDTO.of(this.disclaimerService.getById(id)), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "Get all disclaimer by text")
    public ResponseEntity<List<DisclaimerResponseDTO>> getAllDisclaimerByText(@RequestParam(value = "text", required = false) String text) {
        return new ResponseEntity<>(DisclaimerResponseDTO.of(this.disclaimerService.getAllByText(text)), HttpStatus.OK);
    }

    @RequestMapping(value = "/id/{id}", method = RequestMethod.DELETE)
    @ApiOperation(value = "Delete disclaimer")
    public ResponseEntity deleteDisclaimer(@PathVariable String id) {
        this.disclaimerService.delete(id);
        return ResponseEntity.accepted().build();
    }

    @RequestMapping(value = "/id/{id}", method = RequestMethod.PUT, produces = "application/json")
    @ApiOperation(value = "Update disclaimer")
    public ResponseEntity<DisclaimerResponseDTO> updateDisclaimer(@PathVariable String id, @RequestBody DisclaimerRequestDTO dto) {
        return new ResponseEntity<>(DisclaimerResponseDTO.of(this.disclaimerService.update(id, dto.getName(), dto.getText(), dto.getVersion())), HttpStatus.OK);
    }


}
