package fivvy.delmoralcristian.disclaimer.config.dynamodb;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableDynamoDBRepositories(basePackages = "fivvy.delmoralcristian.disclaimer.repository")
@EntityScan("fivvy.delmoralcristian.disclaimer.model")
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
public class DynamoDBConfig {

    @Value("${dynamodb.endpoint}")
    private String dynamoDbEndpoint;
    @Bean
    public AmazonDynamoDB amazonDynamoDB() {
        AmazonDynamoDB amazonDynamoDB = new AmazonDynamoDBClient();
        amazonDynamoDB.setEndpoint(dynamoDbEndpoint);
        return amazonDynamoDB;
    }

}
