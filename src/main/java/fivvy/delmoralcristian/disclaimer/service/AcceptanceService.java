package fivvy.delmoralcristian.disclaimer.service;

import fivvy.delmoralcristian.disclaimer.exceptions.NotFoundException;
import fivvy.delmoralcristian.disclaimer.model.Acceptance;

import java.util.List;

public interface AcceptanceService {

    Acceptance create(String disclaimerId, Long userId) throws NotFoundException;;


    List<Acceptance> getAllByUserId(Long userId);
}
