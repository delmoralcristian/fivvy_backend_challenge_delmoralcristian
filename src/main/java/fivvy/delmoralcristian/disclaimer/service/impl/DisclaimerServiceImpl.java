package fivvy.delmoralcristian.disclaimer.service.impl;

import fivvy.delmoralcristian.disclaimer.dto.request.DisclaimerRequestDTO;
import fivvy.delmoralcristian.disclaimer.enums.CommonMessage;
import fivvy.delmoralcristian.disclaimer.exceptions.NotFoundException;
import fivvy.delmoralcristian.disclaimer.model.Disclaimer;
import fivvy.delmoralcristian.disclaimer.repository.DisclaimerRepository;
import fivvy.delmoralcristian.disclaimer.service.DisclaimerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@Slf4j
public class DisclaimerServiceImpl implements DisclaimerService {

    @Autowired
    private DisclaimerRepository disclaimerRepository;

    @Override
    public Disclaimer create(String name, String text, Long version) {
        Disclaimer disclaimer = new Disclaimer(name, text, version);
        Disclaimer newDisclaimer = this.disclaimerRepository.save(disclaimer);
        log.info("Disclaimer created - Id: {}", newDisclaimer.getId());
        return newDisclaimer;
    }

    @Override
    public Disclaimer getById(String id) throws NotFoundException {
        return this.getDisclaimerById(id);
    }

    @Override
    public List<Disclaimer> getAllByText(String text) {
        if(Objects.nonNull(text)) {
            log.info("Getting all disclaimers by text: {}", text);
            return this.disclaimerRepository.findByText(text);
        }
        log.info("Getting all disclaimers");
        return (List<Disclaimer>)this.disclaimerRepository.findAll();
    }

    @Override
    public void delete(String id) throws NotFoundException {
        Disclaimer disclaimer = this.getDisclaimerById(id);
        log.info("Deleting disclaimer by id: {}", id);
        this.disclaimerRepository.delete(disclaimer);
    }

    @Override
    public Disclaimer update(String id, String name, String text, Long version) throws NotFoundException {
        Disclaimer disclaimer = this.getDisclaimerById(id);
        log.info("Updating disclaimer by id: {}", id);
        Disclaimer disclaimerToSave = new Disclaimer(id, disclaimer.getCreatedAt(), name, text, version);
        return this.disclaimerRepository.save(disclaimerToSave);
    }

    private Disclaimer getDisclaimerById(String id) {
        log.info("Getting disclaimer by id: {}", id);
        Optional<Disclaimer> disclaimer = this.disclaimerRepository.findById(id);
        if(!disclaimer.isPresent()) {
            throw new NotFoundException(String.format(CommonMessage.DISCLAIMER_NOT_FOUND.getMessage(), id));
        }
        return disclaimer.get();
    }
}
