package fivvy.delmoralcristian.disclaimer.service.impl;

import fivvy.delmoralcristian.disclaimer.exceptions.NotFoundException;
import fivvy.delmoralcristian.disclaimer.model.Acceptance;
import fivvy.delmoralcristian.disclaimer.repository.AcceptanceRepository;
import fivvy.delmoralcristian.disclaimer.service.AcceptanceService;
import fivvy.delmoralcristian.disclaimer.service.DisclaimerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
@Slf4j
public class AcceptanceServiceImpl implements AcceptanceService {

    @Autowired
    AcceptanceRepository acceptanceRepository;

    @Autowired
    DisclaimerService disclaimerService;

    @Override
    public Acceptance create(String disclaimerId, Long userId) throws NotFoundException {
        this.disclaimerService.getById(disclaimerId);
        Acceptance acceptance = new Acceptance(disclaimerId, userId);
        Acceptance newAcceptance = this.acceptanceRepository.save(acceptance);
        log.info("Acceptance created - Id: {}", newAcceptance.getId());
        return newAcceptance;

    }

    @Override
    public List<Acceptance> getAllByUserId(Long userId) {
        if(Objects.nonNull(userId)) {
            log.info("Getting all acceptances by userId: {}", userId);
            return this.acceptanceRepository.findByUserId(userId);
        }
        log.info("Getting all acceptances");
        return (List<Acceptance>)this.acceptanceRepository.findAll();
    }
}
