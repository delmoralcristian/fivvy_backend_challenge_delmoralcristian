package fivvy.delmoralcristian.disclaimer.service;

import fivvy.delmoralcristian.disclaimer.dto.request.DisclaimerRequestDTO;
import fivvy.delmoralcristian.disclaimer.exceptions.NotFoundException;
import fivvy.delmoralcristian.disclaimer.model.Disclaimer;

import java.util.List;

public interface DisclaimerService {

    Disclaimer create(String name, String text, Long version);

    Disclaimer getById(String id) throws NotFoundException;

    List<Disclaimer> getAllByText(String text);

    void delete(String id) throws NotFoundException;

    Disclaimer update(String id, String name, String text, Long version) throws NotFoundException;

}
