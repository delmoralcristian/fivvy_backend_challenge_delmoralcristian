package fivvy.delmoralcristian.disclaimer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DisclaimerManagementApplication {

    public static void main(String[] args) {

        SpringApplication.run(DisclaimerManagementApplication.class, args);

    }
}

