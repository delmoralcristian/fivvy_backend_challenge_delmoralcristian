package fivvy.delmoralcristian.disclaimer.model;

import com.amazonaws.services.dynamodbv2.datamodeling.*;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@EqualsAndHashCode(of = {"id"})
@DynamoDBTable(tableName = "acceptance")
@Getter
@Setter
public class Acceptance {

    @DynamoDBHashKey(attributeName = "id")
    @DynamoDBAutoGeneratedKey
    private String id;
    @DynamoDBAttribute(attributeName = "disclaimerId")
    private String disclaimerId;
    @DynamoDBAttribute(attributeName="userId")
    private Long userId;
    @DynamoDBAttribute(attributeName = "createdAt")
    private Date createdAt;

    public Acceptance() {
    }

    public Acceptance(String disclaimerId, Long userId) {
        this.disclaimerId = disclaimerId;
        this.userId = userId;
        this.createdAt = new Date();
    }

}
