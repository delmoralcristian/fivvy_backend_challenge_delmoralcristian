package fivvy.delmoralcristian.disclaimer.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import fivvy.delmoralcristian.disclaimer.dto.request.DisclaimerRequestDTO;
import fivvy.delmoralcristian.disclaimer.exceptions.NotFoundException;
import fivvy.delmoralcristian.disclaimer.model.Disclaimer;
import fivvy.delmoralcristian.disclaimer.service.DisclaimerService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;

import static org.mockito.ArgumentMatchers.anyString;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = DisclaimerController.class)
@ComponentScan(basePackages = "fivvy.delmoralcristian.disclaimer.service")
public class DisclaimerControllerTest {

    private MockMvc mockMvc;

    @Mock
    private DisclaimerService disclaimerService;

    @InjectMocks
    private DisclaimerController underTest;

    Disclaimer disclaimer;

    String disclaimerId;

    @Before
    public void setUp() {
        disclaimerId = "7f6b3644-0a28-4a15-8b44-56547abfcb6e";
        disclaimer = new Disclaimer("someName", "someText", 1L);
        disclaimer.setId(disclaimerId);
        MockitoAnnotations.openMocks(this);
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(underTest)
                .build();
    }

    @Test
    public void getDisclaimerByIdOK() throws Exception {

        Mockito.when(disclaimerService.getById(anyString())).thenReturn(disclaimer);

        this.mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/disclaimer/id/{id}", disclaimerId)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(disclaimerId));
    }

    @Test
    public void getDisclaimerByIdNotFound() throws Exception {

        Mockito.when(disclaimerService.getById(anyString())).thenThrow(NotFoundException.class);

        this.mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/disclaimer/id/{id}", disclaimerId)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());
    }

    @Test
    public void getDisclaimersByTextOk() throws Exception {

        Mockito.when(disclaimerService.getAllByText(anyString())).thenReturn(Arrays.asList(disclaimer));

        this.mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/disclaimer")
                        .param("text", "someText")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].id").isNotEmpty());
    }

    @Test
    public void getDisclaimersByTexNotFound() throws Exception {
        Mockito.when(disclaimerService.getAllByText(anyString())).thenThrow(NotFoundException.class);

        this.mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/disclaimer")
                        .param("text", "someText")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateDisclaimerOK() throws Exception {

        DisclaimerRequestDTO disclaimerRequestDTO = DisclaimerRequestDTO.builder()
                .name("nameToUpdate")
                .text("textToUpdate")
                .version(1L)
                .build();


        Mockito.when(disclaimerService.update(disclaimerId, "nameToUpdate", "textToUpdate", 1L)).thenReturn(disclaimer);

        this.mockMvc.perform(MockMvcRequestBuilders
                        .put("/api/disclaimer/id/{id}", disclaimerId)
                        .content(asJsonString(disclaimerRequestDTO))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void updateDisclaimerNotFound() throws Exception {

        DisclaimerRequestDTO disclaimerRequestDTO = DisclaimerRequestDTO.builder()
                .name("nameToUpdate")
                .text("textToUpdate")
                .version(1L)
                .build();

        Mockito.when(disclaimerService.update(disclaimerId, "nameToUpdate", "textToUpdate", 1L)).thenThrow(NotFoundException.class);

        this.mockMvc.perform(MockMvcRequestBuilders
                        .put("/api/disclaimer/id/{id}", disclaimerId)
                        .content(asJsonString(disclaimerRequestDTO))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void deleteDisclaimerAccepted() throws Exception
    {
        Mockito.when(disclaimerService.getById(anyString())).thenReturn(disclaimer);

        this.mockMvc.perform(MockMvcRequestBuilders
                        .delete("/api/disclaimer/id/{id}", disclaimerId))
                .andExpect(status().isAccepted());
    }

    @Test
    public void deleteDisclaimerNotFound() throws Exception
    {
        Mockito.doThrow(NotFoundException.class).when(disclaimerService).delete(disclaimerId);

        this.mockMvc.perform(MockMvcRequestBuilders
                        .delete("/api/disclaimer/id/{id}", disclaimerId))
                .andExpect(status().isNotFound());
    }

    private String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}


