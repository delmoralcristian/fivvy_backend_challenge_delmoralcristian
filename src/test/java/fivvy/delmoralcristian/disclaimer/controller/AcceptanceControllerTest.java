package fivvy.delmoralcristian.disclaimer.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import fivvy.delmoralcristian.disclaimer.dto.request.AcceptanceRequestDTO;
import fivvy.delmoralcristian.disclaimer.exceptions.NotFoundException;
import fivvy.delmoralcristian.disclaimer.model.Acceptance;
import fivvy.delmoralcristian.disclaimer.service.AcceptanceService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = AcceptanceController.class)
@ComponentScan(basePackages = "fivvy.delmoralcristian.disclaimer.service")
public class AcceptanceControllerTest {

    private MockMvc mockMvc;

    @Mock
    private AcceptanceService acceptanceService;

    @InjectMocks
    private AcceptanceController underTest;

    Acceptance acceptance;

    String acceptanceId;

    String disclaimerId;

    @Before
    public void setUp() {
        disclaimerId = "9843644-0a28-4a15-8b44-56547abfc456";
        acceptanceId = "7f6b3644-0a28-4a15-8b44-56547abfcb6e";
        acceptance = new Acceptance(disclaimerId, 1L);
        acceptance.setId(acceptanceId);
        MockitoAnnotations.openMocks(this);
        this.mockMvc = MockMvcBuilders
                .standaloneSetup(underTest)
                .build();
    }

    @Test
    public void getAcceptancesByUserIdOk() throws Exception {

        Mockito.when(acceptanceService.getAllByUserId(anyLong())).thenReturn(Arrays.asList(acceptance));

        this.mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/acceptance")
                        .param("userId", "1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").exists())
                .andExpect(MockMvcResultMatchers.jsonPath("$[*].id").isNotEmpty());
    }

    @Test
    public void getAcceptancesByUserIdNotFound() throws Exception {
        Mockito.when(acceptanceService.getAllByUserId(anyLong())).thenThrow(NotFoundException.class);

        this.mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/acceptance")
                        .param("userId", "1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void createAcceptanceOk() throws Exception {
        Mockito.when(acceptanceService.create(disclaimerId, 1L)).thenReturn(acceptance);

        this.mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/acceptance")
                        .content(asJsonString(AcceptanceRequestDTO.builder().disclaimerId(disclaimerId).userId(1L).build()))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    private String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
