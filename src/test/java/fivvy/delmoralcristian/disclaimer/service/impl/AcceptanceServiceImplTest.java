package fivvy.delmoralcristian.disclaimer.service.impl;

import fivvy.delmoralcristian.disclaimer.exceptions.NotFoundException;
import fivvy.delmoralcristian.disclaimer.model.Acceptance;
import fivvy.delmoralcristian.disclaimer.model.Disclaimer;
import fivvy.delmoralcristian.disclaimer.repository.AcceptanceRepository;
import fivvy.delmoralcristian.disclaimer.service.DisclaimerService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;

public class AcceptanceServiceImplTest {

    @Mock
    private AcceptanceRepository acceptanceRepository;

    @Mock
    private DisclaimerService disclaimerService;

    @InjectMocks
    private AcceptanceServiceImpl underTest;

    private Acceptance acceptance;

    private Acceptance acceptance2;

    private Acceptance newAcceptance;


    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        acceptance = new Acceptance("disclaimerId1",  1L);
        acceptance2 = new Acceptance("disclaimerId2",  2L);
        newAcceptance = new Acceptance("disclaimerId3", 3L);
        newAcceptance.setId("7f6b3644-0a28-4a15-8b44-56547abfcb6e");
    }

    @Test
    public void getAllByUserId() {
        Mockito.when(acceptanceRepository.findByUserId(anyLong())).thenReturn(Arrays.asList(acceptance));

        List<Acceptance> response = underTest.getAllByUserId(1L);

        Assert.assertEquals(response.size(), 1);
        Assert.assertEquals(response.get(0).getDisclaimerId(), "disclaimerId1");
    }

    @Test
    public void create() {
        Mockito.when(disclaimerService.getById(anyString())).thenReturn(new Disclaimer());
        Mockito.when(acceptanceRepository.save(acceptance2)).thenReturn(newAcceptance);

        Acceptance response = underTest.create("disclaimerId2", 2L);

        Assert.assertEquals(response.getId(), "7f6b3644-0a28-4a15-8b44-56547abfcb6e");
    }

    @Test(expected = NotFoundException.class)
    public void createNotFound() {
        Mockito.when(disclaimerService.getById(anyString())).thenThrow(NotFoundException.class);

        underTest.create("disclaimerId2", 2L);
    }

}
