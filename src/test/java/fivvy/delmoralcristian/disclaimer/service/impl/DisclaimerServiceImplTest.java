package fivvy.delmoralcristian.disclaimer.service.impl;

import fivvy.delmoralcristian.disclaimer.exceptions.NotFoundException;
import fivvy.delmoralcristian.disclaimer.model.Disclaimer;
import fivvy.delmoralcristian.disclaimer.repository.DisclaimerRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

public class DisclaimerServiceImplTest {

    @Mock
    private DisclaimerRepository disclaimerRepository;

    @InjectMocks
    private DisclaimerServiceImpl underTest;

    private Disclaimer disclaimer;

    private Disclaimer disclaimer2;

    private Disclaimer disclaimer3;


    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        disclaimer = new Disclaimer("disclaimerName", "disclaimerText", 1L);
        disclaimer2 = new Disclaimer("disclaimerName2", "disclaimerText2", 2L);
        disclaimer3 = new Disclaimer("disclaimerName3", "disclaimerText3", 3L);
    }

    @Test
    public void getDisclaimeryId() {

        Mockito.when(disclaimerRepository.findById(anyString())).thenReturn(Optional.of(disclaimer));

        Disclaimer response = underTest.getById("7f6b3644-0a28-4a15-8b44-56547abfcb6e");

        Assert.assertEquals(response.getName(), "disclaimerName");
        Assert.assertEquals(response.getText(),"disclaimerText");
    }

    @Test(expected = NotFoundException.class)
    public void getDisclaimerByIdNotFound() {
        Mockito.when(disclaimerRepository.findById(anyString())).thenReturn(Optional.empty());
        underTest.getById("7f6b3644-0a28-4a15-8b44-56547abfcb6e");
    }

    @Test
    public void getAllByText() {
        Mockito.when(disclaimerRepository.findByText(anyString())).thenReturn(Arrays.asList(disclaimer2));

        List<Disclaimer> response = underTest.getAllByText("disclaimerText2");

        Assert.assertEquals(response.size(), 1);
        Assert.assertEquals(response.get(0).getName(), "disclaimerName2");
    }

    @Test
    public void updateDisclaimer() {


        Mockito.when(disclaimerRepository.save(any())).thenReturn(disclaimer3);
        Mockito.when(disclaimerRepository.findById(anyString())).thenReturn(Optional.of(disclaimer3));

        Disclaimer response = underTest.update("7f6b3644-0a28-4a15-8b44-56547abfcb6e", "nameUpdated", "textUpdated", 5L);

        Assert.assertNotNull(response);
    }

    @Test(expected = NotFoundException.class)
    public void updateDisclaimerNotFound() {


        Mockito.when(disclaimerRepository.save(any())).thenReturn(disclaimer3);
        Mockito.when(disclaimerRepository.findById(anyString())).thenReturn(Optional.empty());

        underTest.update("7f6b3644-0a28-4a15-8b44-56547abfcb6e", "nameUpdated", "textUpdated", 5L);
    }

    @Test(expected = NotFoundException.class)
    public void deleteDisclaimerNotFound() {
        Mockito.when(disclaimerRepository.findById(anyString())).thenReturn(Optional.empty());

        underTest.delete("7f6b3644-0a28-4a15-8b44-56547abfcb6e");
    }


}